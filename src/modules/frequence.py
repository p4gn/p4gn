#!/usr/bin/python
# -*- coding: utf8 -*-

#LISTE DES VALEURS POUR CRITERE
# maj, min, digit, spec, taille

import sys
sys.path.insert(0, '../')
import librairies
import pandas as pd
import numpy

def frequence(csv_file,critere):
	f = open('frequence_result.txt','a')
	f.write('\n'+'--- '+critere+' ---')

	print("--- "+critere+" ---")
	export.append("--- "+critere+" ---")
	csv = pd.read_csv(csv_file, sep=',', error_bad_lines=False)
	column = csv[critere].value_counts()
	liste = dict(column)

	result=[]
	for key,val in liste.items():
		f.write('\n'+str(val)+' mots de '+str(key)+' '+critere)
		result.append(str(key)+','+str(val))
	print()
	export.append(result)

def print_export(export):
	result=[]
	for i in export:
		for key,val in i.items():
			line=str(val)+' mots de '+str(key)+' '+critere
#			print(str(val)+' mots de '+str(key)+' '+critere)
		result.append(line)

def main():
	chemin=librairies.importation()
	print(chemin)
	cheminexport=librairies.choix_chemin()

	global export
	export=[]


	rmin=frequence(chemin,'min')
	rmaj=frequence(chemin,'maj')
	rdigit=frequence(chemin,'digit')
	rspec=frequence(chemin,'spec')
	rtaille=frequence(chemin,'taille')

	librairies.exportation(export,cheminexport)
main()
