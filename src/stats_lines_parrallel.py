import sys
import librairies
import ray

@ray.remote
def calcMaj(contents):
	tabMaj=[]
	cpt=0
	for line in contents :
		cpt +=1
		nbMajuscules = 0
		for c in line :
			if c.isalpha() and c.isupper():
				nbMajuscules += 1
		tabMaj.append(nbMajuscules)
	return(tabMaj)

@ray.remote
def calcMin(contents):
	tabMin=[]
	cpt=0
	for line in contents :
		cpt +=1
		nbMinuscules = 0
		for c in line :
			if c.isalpha() and c.lower():
				nbMinuscules += 1
		tabMin.append(nbMinuscules)
	return(tabMin)

@ray.remote
def calcDigit(contents):
	tabDigits=[]
	cpt=0
	for line in contents :
		cpt +=1
		nbDigits = 0
		for c in line :
			if c.isdigit :
				nbDigits += 1
		tabDigits.append(nbDigits)
	return(tabDigits)

@ray.remote
def calcSpec(contents):
	tabSpec=[]
	cpt=0
	for line in contents :
		cpt +=1
		nbSpec = 0
		for c in line :
			if not c.isdigit and not c.isalpha :
				nbSpec += 1
		tabSpec.append(nbSpec)
	return(tabSpec)


def main():
	# Tableau de sortie
	ray.init()
	result =['mdp,min,maj,digit,spec,taille']

#	f=open(librairies.importation(), "r", encoding = "ISO-8859-1")
	f=open(librairies.importation(), "r")

	chemin=librairies.choix_chemin()

	if f.mode == 'r':
		contents = f.read().splitlines()

		fct1=calcMin.remote(contents)
		fct2=calcMaj.remote(contents)
		fct3=calcDigit.remote(contents)
		fct4=calcSpec.remote(contents)

		tabMin=ray.get(fct1)
		tabMaj=ray.get(fct2)
		tabDigits=ray.get(fct3)
		tabSpec=ray.get(fct4)

		i=0
		for line in contents :
			if line.find(',') == -1 :
				result.append(line+','+str(tabMin[i])+','+str(tabMaj[i])+','+str(tabDigits[i])+','+str(tabSpec[i])+','+str(len(line)))
				i+=1
		print('Nombre total de mots de passe analysés : '+str(i))

		librairies.exportation(result,chemin)
	f.close()


main()
