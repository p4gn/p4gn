#!/usr/bin/python3
#-*-conding:Latin-1-*-

#from tkinter import *
#from tkinter.filedialog import *
import sys
import bd
import string
import librairies

#chem = asksaveasfilename(defaultextension='.csv')
#print(chem)
total = 0
d = {}
f=open(librairies.importation(), "r", encoding = "ISO-8859-1")
if f.mode == 'r':
	for line in f:
		for c in set(line.strip("\n")):
			total += line.count(c)
			if c in d:
				d[c] += line.count(c)
			else:
				d[c] = line.count(c)
"""
for key in sorted(d.keys()):
    print(key,":",d[key])
"""

f.close()
#e.close()

d2 = librairies.moyenne(d)

csv = open("stat_results.csv","w")

csv.write("Nombre Total de caractères = "+str(total)+"\n")
csv.write("Nomenclature : Valeur,Occurrences,TauxOccurrence\n\n")
for key in sorted(d.keys()):
    csv.write(key+" : "+str(d[key])+"\t|\t"+str(d2[key])+" % \n")


csv.close()

print("Analyse terminée, merci d'avoir attendu")
