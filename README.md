# p4gn

![alt text](https://i.imgur.com/Ppzb9ul.png)

P4GN pour "Password for Gendarmerie Nationale" est un générateur intelligent de mots de passe utilisant l'intelligence artificielle.

En cours de développement par le Club des 6.

Revenez plus tard pour de plus amples informations.

Cordialement.

## Liens 
* **Dictionnaires**
    *  [CrackStation](https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm)
    *  [Openwall wordlists](https://www.openwall.com/passwords/wordlists/)
    *  [Liste des mots français](https://github.com/mmai/chiensuperieur/blob/master/dictionnaires/liste.de.mots.francais.frgut.txt)

* **IA en Python (exemples et documentation)**
    *  [Tensorflow](https://www.tensorflow.org/)
    *  [Un exemple d’apprentissage non supervisé en python](https://patducjacquet.wordpress.com/2017/06/04/un-exemple-dapprentissage-non-supervise-en-python/)
    *  [Apprentissage statistique et analyse prédictive en Python avec scikit-learn par Alexandre GRAMFORT](https://www.slideshare.net/Phelion/apprentissage-statistique-et-analyse-prdictive-en-python-avec-scikitlearn-par-alexandre-gramfort) 
    *  [PassGAN : DeepLearning Password Generator](https://github.com/brannondorsey/PassGAN)

## RESSOURCES
*  [Google Drive du projet](https://drive.google.com/open?id=1iiN-7_tnJWV8fK9f__-CRmuTz7NsD9ib)